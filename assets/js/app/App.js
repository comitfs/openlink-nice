//BIND_PATH = '/http-bind';
//BOSH_URL = window.location.protocol + '//' + window.location.hostname + BIND_PATH;
// Use the below three instead if you are not using a proxy to route /http-bind to the correct URL
//BIND_PATH = '/http-bind/';
//BIND_PROTOCOL = window.location.protocol.indexOf('https') > -1? ':7443':':7070';
//BOSH_URL = window.location.protocol + '//' + window.location.hostname + BIND_PROTOCOL + BIND_PATH;
// Use the below if you want to connect based on the domain
//BOSH_URL = 'https://' + document.getElementById('xmppdomain').value +':7443/http-bind/';
//BOSH_URL = 'http://' + document.getElementById('xmpp_domain').value +':7070/http-bind/';

App = {};
Session = {};
App.profiles = [];
App.interests = [];
App.jidInterestMap = [];
App.calls = [];
App.lastCallState = [];
App.recordResults = [];

App.debug = function() {
    if (App.options.app.debug) {
        return true;
    }
    return false;
};

App.clear = function() {
    if (App && Session.connection) {
        Session.connection.disconnect();
        Session.connection = null;
        console.log("XMPP Disconnect");
    }
};

window.onunload = App.clear();

App.signInClick = function() {
    App.clear();

    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    var domain = document.getElementById('xmpp_domain').value;
    var resource = document.getElementById('resource').value;

    // if (App.options.app.cookies) {
    //     var xhttp;
    //     xhttp = new XMLHttpRequest();
    //     xhttp.withCredentials = true;
    //     xhttp.open("POST", "https://" + window.location.hostname + "/cookies", true);
    //     xhttp.onload = function () {
    //         console.log(xhttp.responseText);
    //     };
    //     xhttp.send('TestCookie=7H151547357C00K13');
    // }

    BOSH_URL = 'http://' + document.getElementById('xmpp_domain').value +':7070/http-bind/';
    if (username && password) {
        Session.connection = new Strophe.Connection(BOSH_URL
            ,{
                "keepalive": true
            }
        );
        connect({
            username: username,
            password: password,
            resource: resource,
            domain: domain
        });
    }
};

App.start = function(options) {
    App.options = options;
    console.log('Application Started');
    if (App.options.app.username) {
        document.getElementById('username').value = App.options.app.username;
    }
    if (App.options.app.password) {
        document.getElementById('password').value = App.options.app.password;
    }
    if (App.options.app.xmpp_domain) {
        document.getElementById('xmpp_domain').value = App.options.app.xmpp_domain;
    }
    if (App.options.app.system) {
        document.getElementById('system').value = App.options.app.system;
    }
    if (App.options.app.xmpp_resource) {
        document.getElementById('resource').value = App.options.app.xmpp_resource;
    }
    if (App.options.app.autologon) {
        App.signInClick();
    }
};

var signInButton = document.getElementById('gc_signin');
signInButton.addEventListener('click', App.signInClick);

App.signOutClick = function() {
    Session.connection.disconnect();
}

var signOutButton = document.getElementById('gc_signout');
signOutButton.addEventListener('click', App.signOutClick);

App.connected = function(connection) {
    Session.connection = connection;
    document.getElementById('gc_login_window').style.display = "none";
    document.getElementById('gc_logout_window').style.display = "block";

    var privateData = Session.connection.openlink.getPrivateData(function(response) {
        console.log(response);
    });

    App.discoItems(document.getElementById('xmpp_domain').value);

    App.getGroups();

    Session.connection.openlink.profiles = {};

    Session.connection.openlink.sendPresence();

    Session.callHandlerId = Session.connection.openlink.addCallHandler(App.callHandler);
}

App.disconnected = function() {
    document.getElementById('gc_login_window').style.display = "block";
    document.getElementById('gc_logout_window').style.display = "none";
    // document.getElementById('gc_profile_list').innerHTML = "";
    // document.getElementById('gc_interest_list').innerHTML = "";
    // document.getElementById('gc_feature_list').innerHTML = "";
    // document.getElementById('gc_recording').innerHTML = "";
    document.getElementById('gc_history_ul').innerHTML = "";
    document.getElementById('gc_scheduled_jids').innerHTML = "";
    document.getElementById('systemDropdownUl').innerHTML = "";
    App.profiles = [];
    App.interests = [];

    Session.connection.openlink.removeHandler(Session.callHandlerId);
    delete Session.callHandlerId;
    Session = {};
}

App.callHandler = function(callEv, changed) {
    console.log("CALL HANDLER: ", callEv);
    if (callEv.id) {
        var call = callEv;
        var cid = call.id;

        var scheduledCallElement = document.getElementById('gc_scheduled_call_' + call.id);

        var callText = document.createTextNode(
            call.id + ' - '
            + call.state + ' ');

        if (scheduledCallElement) {
            scheduledCallElement.innerHTML = "";
            scheduledCallElement.appendChild(callText);
            // scheduledCallElement.appendChild(clearConnection);
            // var clearConnectionButton = document.getElementById('gc_clear_connection_' + call.interest + '_' + call.id);
            // clearConnectionButton.addEventListener('click', App.clearConnection);
        } else {
            return;
        }

        if (call.state === "ConnectionCleared") {
            if (document.getElementById('gc_clear_connection_' + call.interest + '_' + call.id)) {
                var clearConnectionElement = document.getElementById('gc_clear_connection_' + call.interest + '_' + call.id);
                clearConnectionElement.parentNode.removeChild(clearConnectionElement);
            }
            scheduledCallElement.className += " alert alert-success";
            var epoch = new Date().getTime();
            var bareJid = App.profiles[call.profile];
            var interestLi = document.getElementById('gc_scheduled_jid_' + bareJid + '_' + call.interest);

            var jidInterestMap = App.jidInterestMap[bareJid]
            if (jidInterestMap && jidInterestMap[call.interest] === 1) {
                return;
            }
            var recordStatusLi = document.createElement('button');
            recordStatusLi.className = "gc_scheduled_call_record_status list-group-item";
            recordStatusLi.type = "button";
            recordStatusLi.dataset.jid = bareJid;
            recordStatusLi.dataset.interest = call.interest;
            if (call.ref && call.ref.gcid) {
                recordStatusLi.id = "gc_scheduled_call_record_status_" + call.id + "_" + epoch + "_" + call.ref.gcid;
            } else {
                recordStatusLi.id = "gc_scheduled_call_record_status_" + call.id + "_" + epoch;
            }
            recordStatusLi.innerHTML = "Get Record Status";
            interestLi.appendChild(recordStatusLi);
            if (call.ref && call.ref.gcid) {
                var callRecordStatusElem = document.getElementById('gc_scheduled_call_record_status_' + call.id + '_' + epoch + '_' + call.ref.gcid);
            } else {
                var callRecordStatusElem = document.getElementById('gc_scheduled_call_record_status_' + call.id + '_' + epoch);
            }
            callRecordStatusElem.addEventListener('click', App.getRecordStatus);
            
            var autoGetRecordStatus = document.getElementById('auto_get_record_status').value;
            if (autoGetRecordStatus && autoGetRecordStatus !== "0") {
                setTimeout(function() {
                    callRecordStatusElem.click();
                }, autoGetRecordStatus * 1000);
            }
            if (jidInterestMap) {
                jidInterestMap[call.interest] = 1;
                console.log(App.jidInterestMap);
                for (elem in jidInterestMap) {
                    var interest = jidInterestMap[elem];
                    if (interest === 0) {
                        console.log(elem);
                        App.scheduledMakeCall(elem, bareJid);
                        return;
                    }
                }
            }
        }
    }
}

App.scheduledTest = function(e) {
    e.preventDefault();
    var system = getDefaultSystem();

    var resource = document.getElementById('resource').value;
    var bareJidList = document.getElementById('control_username').value? document.getElementById('control_username').value:Session.connection.jid;
    bareJidList = bareJidList.toLowerCase();
    bareJidList = bareJidList.replace(/\s/g,'');
    bareJidList = bareJidList.split(',');
    var bareJidArray = [];
    for (var _i = 0, _len = bareJidList.length; _i < _len; _i++) {
        bareJidArray.push(bareJidList[_i]);
    }
    var destination = document.getElementById('scheduled_destination').value;
    var testCallDuration = document.getElementById('scheduled_call_time').value;
    var testScheduledTime = document.getElementById('scheduled_time').value;
    
    if (e.target.id) {
        var timer = e.target.id.replace('gc_', '');
        timer = timer.replace('_test_btn', '');
    }

    if (timer && timer === "scheduled") {
        var currentDateTime = new Date().getTime();
        var scheduledDateTime = new Date(testScheduledTime).getTime();
        var scheduledTime = scheduledDateTime - currentDateTime;
        scheduledTime = scheduledTime/1000;
        console.log(scheduledTime);
    } else {
        var scheduledTime = 0;
    }

    var testJidsDiv = document.getElementById('gc_scheduled_jids');

    if (!document.getElementById('gc_scheduled_jids_collapse')) {
        var collapseButton = document.createElement('button');
        collapseButton.type = "button";
        collapseButton.className = "btn btn-primary";
        collapseButton.id = "gc_scheduled_jids_collapse";
        collapseButton.innerHTML = "Collapse All";
        testJidsDiv.prepend(collapseButton);
        var jidsCollapseButton = document.getElementById('gc_scheduled_jids_collapse');
        jidsCollapseButton.addEventListener('click', App.scheduledJidsCollapse);
    }
    
    for (var _i = 0, _len = bareJidArray.length; _i < _len; _i++) {
        var bareJid = bareJidArray[_i];
        bareJid = bareJid.split('@');
        bareJid = bareJid[0] + "@" + document.getElementById('xmpp_domain').value;

        if (document.getElementById('gc_panel_' + bareJid)) {
            testJidsDiv.removeChild(document.getElementById('gc_panel_' + bareJid));
        }
        if (testJidsDiv) {
            var splitJid = bareJid.split('@');
            var splitJidUser = splitJid[0];
            var splitJidServer = splitJid[1];
            splitJidServer = splitJidServer.split('.');
            splitJidServer = splitJidServer[0];
            var encodedJid = splitJidUser + "-" + splitJidServer;

            var panelDiv = document.createElement('div');
            panelDiv.className = "panel panel-default";
            panelDiv.id = "gc_panel_" + bareJid;
            var panelHeading = document.createElement('div');
            panelHeading.className = "panel-heading";
            panelHeading.role = "tab";
            panelHeading.id = "gc_panel_heading_" + bareJid;
            var panelHeadingA = document.createElement('a');
            panelHeadingA.role = "button";
            panelHeadingA.id = "gc_panel_heading_a_" + bareJid;
            panelHeadingA.href = "#gc_panel_body_" + encodedJid;
            //panelHeadingA.dataset.parent = "#gc_scheduled_jids";
            panelHeadingA.dataset.toggle = "collapse";
            panelHeadingA.setAttribute('aria-controls', 'gc_panel_body_' + encodedJid);
            panelHeadingA.setAttribute('aria-expanded', true);
            panelHeadingA.innerHTML = "User JID: " + bareJid + " ";

            App.recordResults[bareJid] = {};
            App.recordResults[bareJid].success = 0;
            App.recordResults[bareJid].failure = 0;
            console.log(App.recordResults);
            var spanSuccess = document.createElement('span');
            spanSuccess.className = "badge badge-success";
            spanSuccess.id = "gc_panel_heading_a_success_" + bareJid;
            spanSuccess.innerHTML = App.recordResults[bareJid].success;
            var spanFailure = document.createElement('span');
            spanFailure.className = "badge badge-danger"
            spanFailure.id = "gc_panel_heading_a_failure_" + bareJid;
            spanFailure.innerHTML = App.recordResults[bareJid].failure;
            panelHeading.appendChild(panelHeadingA);
            panelHeading.appendChild(spanSuccess);
            panelHeading.appendChild(spanFailure);
            panelDiv.appendChild(panelHeading);

            var panelBody = document.createElement('div');
            panelBody.className = "gc_collapse_jid panel-collapse collapse in";
            panelBody.role = "tabpanel";
            panelBody.id = "gc_panel_body_" + encodedJid;
            panelBody.setAttribute('aria-labelledby', 'gc_panel_heading_' + bareJid);
            var panelBodySub = document.createElement('div');
            panelBodySub.className = "panel-body";
            panelBodySub.id = "gc_panel_body_sub_" + bareJid;
            panelBody.appendChild(panelBodySub)
            panelDiv.appendChild(panelBody);
            testJidsDiv.appendChild(panelDiv);
        }

        var jidUl = document.getElementById('gc_panel_body_sub_' + bareJid);
        
        var errorLi = document.createElement('li');
        errorLi.className = "gc_scheduled_error list-group-item alert alert-danger";
        errorLi.id = "gc_scheduled_error" + bareJid;

        if (!destination || !testCallDuration || (timer && timer === "scheduled" && !scheduledTime)) {
            errorLi.innerHTML = "Please ensure you enter a Destination, Duration and Scheduled time";
            jidUl.appendChild(errorLi);
            return;
        }
        if (scheduledTime < 0) {
            errorLi.innerHTML = "Please select a schedule date and time in the future";
            jidUl.appendChild(errorLi);
            return;
        }

        var scheduledLi = document.createElement('li');
        scheduledLi.className = "gc_scheduled_schedule list-group-item alert alert-info";
        scheduledLi.id = "gc_scheduled_jid_" + bareJid + "_scheduled";
        scheduledLi.innerHTML = "Test scheduled to run at: " + testScheduledTime + ", in " + scheduledTime + " seconds";
        jidUl.appendChild(scheduledLi);

        App.scheduledTestTimeout(bareJid, scheduledTime);
    }
}

App.scheduledTestTimeout = function(bareJid, scheduledTime) {
    var resource = document.getElementById('resource').value;
    var jidUl = document.getElementById('gc_panel_body_sub_' + bareJid);
    setTimeout(function() {
        var scheduledElement = document.getElementById('gc_scheduled_jid_' + bareJid + '_scheduled');
        if (scheduledElement) {
            scheduledElement.parentNode.removeChild(scheduledElement);   
        }
        App.profiles = [];
        App.interests = [];
        App.jidInterestMap[bareJid] = {}
        var errorLi = document.createElement('li');
        errorLi.className = "gc_scheduled_error list-group-item alert alert-danger";
        errorLi.id = "gc_scheduled_error" + bareJid;
        Session.connection.openlink.getProfiles(getDefaultSystem(), resource, "", "", "", function(profiles) {
            for (var profileId in profiles) {
                var profile = profiles[profileId];
                if (profile && profile.default === "true") {
                    App.profiles[bareJid] = profile.id;
                }
                App.profiles[profile.id] = bareJid;   
            }
            var defaultProfile = App.profiles[bareJid];
            var profileLi = document.createElement('li');
            profileLi.className = "gc_scheduled_profile list-group-item alert alert-success";
            profileLi.id = "gc_scheduled_jid_" + bareJid + "_" + defaultProfile;
            profileLi.innerHTML = "Profile: " + defaultProfile;
            jidUl.appendChild(profileLi);
            Session.connection.openlink.getInterests(getDefaultSystem(), defaultProfile, function(interests) {
                console.log(interests);
                for (var elem in interests) {
                    var interest = interests[elem];
                    if (interest) {
                        App.interests[bareJid] = [];
                        App.interests[bareJid].push(interest.id);
                        App.jidInterestMap[bareJid][interest.id] = 0;
                        var interestLi = document.createElement('li');
                        interestLi.className = "gc_scheduled_interest list-group-item alert alert-success";
                        interestLi.id = "gc_scheduled_jid_" + bareJid + "_" + interest.id;
                        var device = interest.id.split('SEP');
                        device = "SEP" + device[1];
                        interestLi.innerHTML = "Interest: " + interest.id + ", Device: " + device;
                        jidUl.appendChild(interestLi);
                    }
                }
                console.log("INTERESTS: ", App.interests);
                console.log("INTEREST JID MAP: ", App.jidInterestMap);
                var defaultInterest = App.interests[bareJid];
                App.scheduledMakeCall(defaultInterest, bareJid);
            }, function(message) {
                console.log("ALERT:",message);
                if (message && message["errorIq"]) {
                    var errorType = "IQ Error";
                    var errorText = message["errorIq"];
                } else if (message && message["errorNote"]){
                    var errorType = "Note Error";
                    var errorText = message["errorNote"];
                }
                errorLi.innerHTML = "Get Interests " + errorType + ": " + errorText;
                jidUl.appendChild(errorLi); 
            });
        }, function(message) {
            console.log("ALERT:",message);
            if (message && message["errorIq"]) {
                var errorType = "IQ Error";
                var errorText = message["errorIq"];
            } else if (message && message["errorNote"]){
                var errorType = "Note Error";
                var errorText = message["errorNote"];
            }
            errorLi.innerHTML = "Get Profiles " + errorType + ": " + errorText;
            jidUl.appendChild(errorLi); 
        }, bareJid);
    }, scheduledTime * 1000);
}

App.scheduledMakeCall = function(interest, bareJid) {
    var system = getDefaultSystem();
    var destination = document.getElementById('scheduled_destination').value;
    var defaultInterest = interest;
    var testCallDuration = document.getElementById('scheduled_call_time').value;
    App.subscribe(defaultInterest);
    Session.connection.openlink.makeCall(system, defaultInterest, destination,
    '', function(call) {
        console.log("ALERT:",'Call made with id: ' + call.id);
        var interestLi = document.getElementById('gc_scheduled_jid_' + bareJid + '_' + defaultInterest);
        var callDetailsLi = document.createElement('li');
        callDetailsLi.className = "gc_scheduled_call list-group-item";
        callDetailsLi.id = "gc_scheduled_call_" + call.id;
        callDetailsLi.innerHTML = "Call being made...";
        interestLi.appendChild(callDetailsLi);
        setTimeout(function() {
            var action = new Action({
                id: "ClearConnection",
                value1: "",
                value2: ""
            });
            Session.connection.openlink.requestAction(system, defaultInterest, call.id, action, function(call) {
                if (call) {
                    console.log("ALERT:",'Call actioned with id: ' + call.id);
                }
            },function(message) {
                console.log("ALERT:",message);
                errorLi.innerHTML = "Request Action ERROR: " + message.error;
                jidUl.appendChild(errorLi); 
            });
        }, testCallDuration * 1000)
    },function(message) {
        console.log("ALERT:",message);
        if (message && message["errorIq"]) {
            var errorType = "IQ Error";
            var errorText = message["errorIq"];
        } else if (message && message["errorNote"]){
            var errorType = "Note Error";
            var errorText = message["errorNote"];
        }
        var interestLi = document.getElementById('gc_scheduled_jid_' + bareJid + '_' + defaultInterest);
        var errorLi = document.createElement('li');
        errorLi.className = "gc_scheduled_error list-group-item alert alert-danger";
        errorLi.id = "gc_scheduled_error_" + bareJid + "_" + defaultInterest;
        errorLi.innerHTML = "Make Call " + errorType + ": " + errorText;
        interestLi.appendChild(errorLi); 

        if (App.jidInterestMap && App.jidInterestMap[bareJid]) {
            var jidInterestMap = App.jidInterestMap[bareJid]
            jidInterestMap[defaultInterest] = 1;
            console.log(App.jidInterestMap);
            for (elem in jidInterestMap) {
                var interest = jidInterestMap[elem];
                if (interest === 0) {
                    console.log(elem);
                    App.scheduledMakeCall(elem, bareJid);
                    return;
                }
            }
        }
    }, bareJid);
}

var immediateTestButton = document.getElementById('gc_immediate_test_btn');
immediateTestButton.addEventListener('click', App.scheduledTest);

var scheduledTestButton = document.getElementById('gc_scheduled_test_btn');
scheduledTestButton.addEventListener('click', App.scheduledTest);

App.subscribe = function(interest) {
    if (interest) {
        var interest = interest;
    } else {
        var interest = App.interests[0];
    }
    Session.connection.openlink.subscribe(Session.connection.openlink.getPubsubAddress(), interest, function(message) {
        console.log("ALERT:",message);
    }, function(message) {
        console.log("ALERT:",message);
    });
};

App.unsubscribe = function(interest) {
    if (interest) {
        var interest = interest;
    } else {
        var interest = App.interests[0];
    }
    var getSubscriptionsSuccessCallBack = function(subscriptions) {
        if (subscriptions.length > 0) {
            for (var _i = 0, _len = subscriptions.length; _i < _len; _i++) {
                Session.connection.openlink.unsubscribe(Session.connection.openlink.getPubsubAddress(), interest, subscriptions[_i].subid,
                function(message) {
                    console.log("ALERT:", message);
                }, function(message) {
                    console.log("ERROR ALERT:", message);
                }) 
            }
        } else {
            console.log('No subscriptions exist');
        }
    }

    var getSubscriptionsErrorCallBack = function(message) {
        console.log("getSubscriptions Error:", message)
    }

    App.getSubscriptions(interest, getSubscriptionsSuccessCallBack, getSubscriptionsErrorCallBack);
};

App.getRecordStatus = function(e) {
    e.preventDefault();
    if (e.target.id) {
        var deviceCallidEpochGcid = e.target.id.replace('gc_scheduled_call_record_status_', '');
        var deviceCallidEpochGcidSplit = deviceCallidEpochGcid.split('SEP');

        var interest = deviceCallidEpochGcidSplit[0];
        var device = interest.split('C');
        device = device[1];

        var callidEpochGcid = deviceCallidEpochGcidSplit[1];
        callidEpochGcidSplit = callidEpochGcid.split("-");
        callidEpochGcid = callidEpochGcidSplit[1];
        var callidEpochGcidSplit = callidEpochGcid.split("_");
        var ctiCallId = callidEpochGcidSplit[0];
        var epochTime = callidEpochGcidSplit[1]/1000;
        var gcid = callidEpochGcidSplit[2];
    }
    if (e.target.dataset) {
        var bareJid = e.target.dataset.jid;
        var interest = e.target.dataset.interest;
    }
    var ntrTimeZone = document.getElementById('ntrTimeZone').value;
    var testCallDuration = Number(document.getElementById('scheduled_call_time').value);
    if (new Date()) {
        var startDate = new Date((epochTime-600)*1000);
        var utcOffset = startDate.getTimezoneOffset();
        if (startDate) {
            if (ntrTimeZone === "utc") {
                startDate = new Date((epochTime-600+(utcOffset*60))*1000);
            }
            var startDay = startDate.getDate();
            startDay = startDay < 10? '0'+startDay:startDay;
            var month = startDate.getMonth() + 1;
            month = month < 10? '0'+month:month;
            var year = startDate.getFullYear();
            var hour = startDate.getHours();
            hour = hour < 10? '0'+hour:hour;
            var minute = startDate.getMinutes();
            minute = minute < 10? '0'+minute:minute;
            var second = startDate.getSeconds();
            second = second < 10? '0'+second:second;
            var millisecond = startDate.getMilliseconds();
            millisecond = millisecond < 10? '00'+millisecond:(millisecond < 100? '0'+millisecond:millisecond);
            startDate = year+'-'+month+'-'+startDay+' '+hour+':'+minute+':'+second;
        }
        var endDate = new Date((epochTime+600)*1000);
        if (endDate) {
            if (ntrTimeZone === "utc") {
                endDate = new Date((epochTime+600+(utcOffset*60))*1000);
            }
            var endDay = endDate.getDate();
            endDay = endDay < 10? '0'+endDay:endDay;
            var endMonth = endDate.getMonth() + 1;
            endMonth = endMonth < 10? '0'+endMonth:endMonth;
            var endYear = endDate.getFullYear();
            var endHour = endDate.getHours();
            endHour = endHour < 10? '0'+endHour:endHour;
            var endMinute = endDate.getMinutes();
            endMinute = endMinute < 10? '0'+endMinute:endMinute;
            var endSecond = endDate.getSeconds();
            endSecond = endSecond < 10? '0'+endSecond:endSecond;
            var endMillisecond = endDate.getMilliseconds();
            endMillisecond = endMillisecond < 10? '00'+endMillisecond:(endMillisecond < 100? '0'+endMillisecond:endMillisecond);
            endDate = endYear+'-'+endMonth+'-'+endDay+' '+endHour+':'+endMinute+':'+endSecond;
        }
    }

    var toggleUrl = document.getElementById('toggleUrl').value;
    if (toggleUrl === "cticallid") {
        var url = "http://" + window.location.hostname + "/vradaptor?action=3&cticallid=" + gcid + "&starttime=" + startDate + "&endtime=" + endDate;
    } else {
        var url = "http://" + window.location.hostname + "/vradaptor?action=3&device=" + device + "&starttime=" + startDate + "&endtime=" + endDate;
    }

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.withCredentials = true;
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            var response = xmlHttp.responseText;
            console.log(response);
            var xml = $.parseXML(response);
            var responseData = Session.connection.openlink._parseVRXml(xml);
            console.log(responseData);
            if (!App.recordResults[bareJid][interest]) {
                App.recordResults[bareJid][interest] = {};
            } else {
                if (App.recordResults[bareJid][interest].success) {
                    App.recordResults[bareJid].success -= 1;
                } else if (App.recordResults[bareJid][interest].failure) {
                    App.recordResults[bareJid].failure -= 1;
                }
            }
            if (responseData.status === "successful") {
                console.log("Success! Sequence id: " + responseData.vrcalls.vrcall.cvskey + " - GCID: " + responseData.vrcalls.vrcall.CTICallid + " - Voice Metric: " + responseData.vrcalls.vrcall.CVSC96);
                var responseStatusBtn = document.getElementById('gc_scheduled_call_record_status_' + deviceCallidEpochGcid);
                responseStatusBtn.innerHTML = "Get Record Status - SUCCESS";
                responseStatusBtn.innerHTML += " - Sequence id: " + responseData.vrcalls.vrcall.cvskey;
                responseStatusBtn.innerHTML += " - GCID: " + responseData.vrcalls.vrcall.CTICallid;
                responseStatusBtn.innerHTML += " - Voice Metric: " + responseData.vrcalls.vrcall.CVSC96;
                responseStatusBtn.className = "gc_scheduled_call_record_status list-group-item";
                responseStatusBtn.className += " alert alert-success";
                App.recordResults[bareJid][interest].success = true;
                App.recordResults[bareJid].success += 1;
            } else {
                console.log("Failure! Reason: " + responseData.cause);
                var responseStatusBtn = document.getElementById('gc_scheduled_call_record_status_' + deviceCallidEpochGcid);
                responseStatusBtn.innerHTML = "Get Record Status - FAILURE - " + responseData.cause;
                responseStatusBtn.className = "gc_scheduled_call_record_status list-group-item";
                responseStatusBtn.className += " alert alert-danger";
                App.recordResults[bareJid][interest].failure = true
                App.recordResults[bareJid].failure += 1;
            }
            document.getElementById('gc_panel_heading_a_success_' + bareJid).innerHTML = App.recordResults[bareJid].success;
            document.getElementById('gc_panel_heading_a_failure_' + bareJid).innerHTML = App.recordResults[bareJid].failure;
        } else if (xmlHttp.readyState == 4) {
            console.log("Something has gone wrong", xmlHttp.responseText);
        }
    }
    xmlHttp.open("GET", url, true); // true for asynchronous 
    xmlHttp.send(null);
}

App.getRecordSummary = function(e) {
    e.preventDefault();

    var successArray = document.getElementsByClassName('gc_scheduled_call_record_status alert-success');
    var failureArray = document.getElementsByClassName('gc_scheduled_call_record_status alert-danger');
    console.log(successArray, failureArray);

    var recordSummaryDiv = document.getElementById('gc_recording_summary');
    if (document.getElementById('gc_panel_record_summary')) {
        recordSummaryDiv.removeChild(document.getElementById('gc_panel_record_summary'));
    }
    if (recordSummaryDiv) {
        var panelDiv = document.createElement('div');
        panelDiv.className = "panel panel-default";
        panelDiv.id = "gc_panel_record_summary";
        var panelHeading = document.createElement('div');
        panelHeading.className = "panel-heading";
        panelHeading.role = "tab";
        panelHeading.id = "gc_panel_heading_record_summary";
        var panelHeadingA = document.createElement('a');
        panelHeadingA.role = "button";
        panelHeadingA.id = "gc_panel_heading_a_record_summary";
        panelHeadingA.href = "#gc_panel_body_record_summary";
        //panelHeadingA.dataset.parent = "#gc_scheduled_jids";
        panelHeadingA.dataset.toggle = "collapse";
        panelHeadingA.setAttribute('aria-controls', 'gc_panel_body_record_summary');
        panelHeadingA.setAttribute('aria-expanded', true);
        panelHeadingA.innerHTML = "Report Summary";
        panelHeading.appendChild(panelHeadingA);
        panelDiv.appendChild(panelHeading);

        var panelBody = document.createElement('div');
        panelBody.className = "panel-collapse collapse in";
        panelBody.role = "tabpanel";
        panelBody.id = "gc_panel_body_record_summary";
        panelBody.setAttribute('aria-labelledby', 'gc_panel_heading_record_summary');
        var panelBodySub = document.createElement('div');
        panelBodySub.className = "panel-body";
        panelBodySub.id = "gc_panel_body_sub_record_summary";
        panelBody.appendChild(panelBodySub)
        panelDiv.appendChild(panelBody);
        recordSummaryDiv.appendChild(panelDiv);
    }
    if (document.getElementById('gc_panel_body_sub_record_summary')) {
        var recordSummaryBody = document.getElementById('gc_panel_body_sub_record_summary');
        if (successArray) {
            var successCount = 0;
            for (var _i = 0, _len = successArray.length; _i < _len; _i++) {
                if (successArray[_i]) {
                    successCount = successCount + 1;
                }
            }
            var liSuccessElement = document.createElement('li');
            liSuccessElement.className = "list-group-item alert alert-success";
            liSuccessElement.innerHTML = "Successful Recordings: ";
            var spanSuccessElement = document.createElement('span');
            spanSuccessElement.className = "badge badge-success"
            spanSuccessElement.innerHTML = successCount;
            liSuccessElement.appendChild(spanSuccessElement);
            recordSummaryBody.appendChild(liSuccessElement);
        }
        if (failureArray) {
            var failureCount = 0;
            for (var _j = 0, _len = failureArray.length; _j < _len; _j++) {
                if (failureArray[_j]) {
                    failureCount = failureCount + 1;
                }
            }
            var liFailureElement = document.createElement('li');
            liFailureElement.className = "list-group-item alert alert-danger";
            liFailureElement.innerHTML = "Failed Recordings: ";
            var spanFailureElement = document.createElement('span');
            spanFailureElement.className = "badge badge-danger"
            spanFailureElement.innerHTML = failureCount;
            liFailureElement.appendChild(spanFailureElement);
            recordSummaryBody.appendChild(liFailureElement);
        }
    }
}
var getRecordSummaryButton = document.getElementById('gc_get_record_summary');
getRecordSummaryButton.addEventListener('click', App.getRecordSummary);

App.scheduledJidsCollapse = function(e) {
    e.preventDefault();
    var jidElements = document.getElementsByClassName('gc_collapse_jid');
    for (var _i = 0, _len = jidElements.length; _i < _len; _i++) {
        jidElements[_i].classList.remove('in');
    }
}

App.getCallHistory = function() {
    document.getElementById('gc_history_ul').innerHTML = "";
    var ulElement = document.getElementById('gc_history_ul');
    var liElement = document.createElement('li');
    liElement.id = "gc_history_loading";
    var liText = document.createTextNode('Loading History');
    liElement.appendChild(liText);
    ulElement.appendChild(liElement);
    document.getElementById('gc_history_list').appendChild(ulElement);
    var bareJid = document.getElementById('control_username').value? document.getElementById('control_username').value + '@' + App.options.app.xmpp_domain:'';
    bareJid = bareJid.toLowerCase();
    var profile = bareJid.split('@')[0];
        
    Session.connection.openlink.getCallHistory(getDefaultSystem(), bareJid, 
        profile, "", "", "all", "", "", "", "5", function(history) {
        for (var k in history) { historyValue = history[k]; break; }
        var historyTsc = historyValue.tsc;

        if (document.getElementById('gc_history_loading')) {
            loadingElement = document.getElementById('gc_history_loading');
            loadingElement.parentNode.removeChild(loadingElement);
        }
        if (document.getElementById('history_' + historyTsc)) {
            historyTscElement = document.getElementById('history_' + historyTsc);
            historyTscElement.parentNode.removeChild(historyTscElement);
        }
        var historyTscDiv = document.createElement('div');
        historyTscDiv.id = "history_" + historyTsc;
        var historyTscUl = document.createElement('ul');
        var historyTscLi = document.createElement('li');
        var historyTscLiName = document.createTextNode(historyTsc);
        historyTscLi.appendChild(historyTscLiName);
        var historyListUl = document.createElement('ul');

        if (history) {
            console.log(history);
        } else {
            return;
        }

        for (var callid in history) {
            var call = history[callid];

            var historyListLi = document.createElement('li');
            var historyText = document.createTextNode(call.id);
            historyListLi.appendChild(historyText);

            var historyUl = document.createElement('ul');

            for (var property in call) {
                var historyPropertyLi = document.createElement('li');
                var historyPropertyText = document.createTextNode(property + ': ' + call[property]);

                historyPropertyLi.appendChild(historyPropertyText);
                historyUl.appendChild(historyPropertyLi);
            }

            historyListLi.appendChild(historyUl);
            historyListUl.appendChild(historyListLi);
        }
        historyTscLi.appendChild(historyListUl);
        historyTscDiv.appendChild(historyTscLi);

        document.getElementById('gc_history_list').appendChild(historyTscDiv);
    },function(message) {
        if (document.getElementById('gc_history_loading')) {
            var defaultElement = document.getElementById('gc_history_loading');
            defaultElement.parentNode.removeChild(defaultElement);
        }
        console.log("ALERT:", message);
        if (message && message["errorIq"]) {
            var errorType = "IQ Error";
            var errorText = message["errorIq"];
        } else if (message && message["errorNote"]){
            var errorType = "Note Error";
            var errorText = message["errorNote"];
        }
        var liElement = document.createElement('li');
        liElement.id = "gc_history_error_" + message["from"];
        var liText = document.createTextNode(errorType + ' whilst getting history: ' + errorText);
        liElement.appendChild(liText);
        ulElement.appendChild(liElement);
        document.getElementById('gc_history_list').appendChild(ulElement);
    });
};

// var getCallHistoryButton = document.getElementById('gc_get_history');
// getCallHistoryButton.addEventListener('click', App.getCallHistory);

App.discoItems = function(to) {
    var to = to? to:document.getElementById('xmpp_domain').value;
    Session.connection.openlink.discoItems(to, function(response) {
        console.log(response);
        var items = response.getElementsByTagName('query')[0].childNodes;
        for (var i = 0; i < items.length; i++) {
            var data = {};
            for (var j = 0; j < items[i].attributes.length; j++){
                data[ items[i].attributes[j].nodeName ] =  items[i].attributes[j].nodeValue;
            };
            App.discoInfo(data.jid);
        }
    }, function(response) {
        console.log(response);
    });
}

App.discoInfo = function(to) {
    Session.connection.openlink.discoInfo(to, function(response) {
        // console.log(response);
        // for (var i=0; i < response.length; i++) {
        //     var attributes = response[_i];
        // }
        var features = response.getElementsByTagName('query')[0].childNodes;
        for (var i = 0; i < features.length; i++) {
            var data = {};

            for (var j = 0; j < features[i].attributes.length; j++){
                data[ features[i].attributes[j].nodeName ] =  features[i].attributes[j].nodeValue;
            };

            data.node_name = features[i].nodeName;
            data.from = response.getAttribute('from');
            data.xml = features[i];

            if (data.var) {
                if (data.var.indexOf("gtx/telephony") > -1) {
                    data.type = "gtx";
                } else if (data.var.indexOf("openlink:01:00:00#tsc") > -1) {
                    data.type = "openlink";
                }
                if (data.type) {
                    data.system = data.from.replace("." + Session.connection.domain, '');
                    var dropdownUl = document.getElementById('systemDropdownUl');
                    var dropdownLi = document.createElement('li');
                    dropdownLi.id = "systemDropdownLi_" + data.system;
                    var dropdownA = document.createElement('a');
                    dropdownA.href = "#";
                    dropdownA.dataset.value = data.system;
                    dropdownA.innerHTML = data.system;
                    dropdownLi.appendChild(dropdownA);
                    dropdownUl.appendChild(dropdownLi);
                }
            }
        }
        $(".dropdown-menu li a").click(function(){
          $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
          $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
        });
    }, function(response) {
        console.log(response);
    });
}

App.getGroups = function() {
    var url = "http://" + window.location.hostname + "/rest/api/restapi/v1/groups";
    //var url = "http://" + window.location.hostname + "/restapi/index.html";
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.withCredentials = true;
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            var response = xmlHttp.responseText;
            var xmlResponse = $.parseXML(response);
            var groups = Session.connection.openlink._parseGroupXml(xmlResponse);
            console.log(groups);
            var dropdownUl = document.getElementById('ofGroupsUl');
            dropdownUl.innerHTML = "";
            for (var _i = 0; _i < groups.length; _i++) {
                var group = groups[_i];
                var dropdownLi = document.createElement('li');
                dropdownLi.id = "ofGroupsLi_" + group.name;
                var dropdownA = document.createElement('a');
                dropdownA.href = "#";
                dropdownA.dataset.value = group.name;
                dropdownA.innerHTML = group.name;
                dropdownLi.appendChild(dropdownA);
                dropdownUl.appendChild(dropdownLi);
                var getGroupA = document.getElementById('ofGroupsLi_' + group.name);
                getGroupA.addEventListener('click', App.getGroupMembers);
            }
            $(".dropdown-menu li a").click(function(){
              $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
              $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
            });
        } else if (xmlHttp.readyState == 4) {
            console.log("Something has gone wrong", xmlHttp.responseText);
        }
    }
    xmlHttp.open("GET", url, true); // true for asynchronous
    xmlHttp.setRequestHeader("Content-Type", "application/json");
    xmlHttp.setRequestHeader("Authorization", "Es103tc04aO0k4j1");
    // xmlHttp.setRequestHeader("Access-Control-Allow-Origin", "*");
    // xmlHttp.setRequestHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
    // xmlHttp.setRequestHeader("Access-Control-Allow-Headers", "Origin, Content-Type, X-Auth-Token");
    xmlHttp.send();
}

App.getGroupMembers = function(e) {
    e.preventDefault();
    var groupName = e.target.dataset.value;
    var url = "http://" + window.location.hostname + "/rest/api/restapi/v1/groups/" + groupName;
    //var url = "http://" + window.location.hostname + "/restapi/index.html";
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.withCredentials = true;
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            var response = xmlHttp.responseText;
            var xmlResponse = $.parseXML(response);
            var members = Session.connection.openlink._parseMemberXml(xmlResponse);
            console.log(members);
            var usernameField = document.getElementById("control_username");
            var jidList = usernameField.value;
            var currentJids = jidList.toLowerCase();
            currentJids = currentJids.replace(/\s/g,'');
            currentJids = currentJids.split(',');
            for (var _i = 0; _i < members.length; _i++) {
                if (!currentJids.includes(members[_i])) {
                    jidList += jidList?"," + members[_i]:members[_i];
                }
            }
            usernameField.value = jidList;
        } else if (xmlHttp.readyState == 4) {
            console.log("Something has gone wrong", xmlHttp.responseText);
        }
    }
    xmlHttp.open("GET", url, true); // true for asynchronous
    xmlHttp.setRequestHeader("Content-Type", "application/json");
    xmlHttp.setRequestHeader("Authorization", "Es103tc04aO0k4j1");
    // xmlHttp.setRequestHeader("Access-Control-Allow-Origin", "*");
    // xmlHttp.setRequestHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
    // xmlHttp.setRequestHeader("Access-Control-Allow-Headers", "Origin, Content-Type, X-Auth-Token");
    xmlHttp.send();
}

$(function () {
    // $(".dropdown#autoClearDropdown .dropdown-menu li a")[0].click();
    $(".dropdown#queryDropdown .dropdown-menu li a")[0].click();
    $('#datetimepicker1').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        minDate: moment()
    });
});

$(".dropdown-menu li a").click(function(){
  $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
  $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
});

function getDefaultSystem() {
    return document.getElementById('system').value + '.' + Session.connection.domain;
}

function getVMSSystem() {
    return 'vmstsp.' + Session.connection.domain;
}

function connect(data) {
    console.log("Connect to: " + BOSH_URL);

    Session.connection.rawInput = function(body) {
        if (App.debug()) {
            console.log('RECV: ' + body);
        }
    };

    Session.connection.rawOutput = function(body) {
        if (App.debug()) {
            console.log('SENT: ' + body);
        }
    };

    var connectionCallback = function(status, condition) {
        console.log(status, condition);
        if (status == Strophe.Status.ERROR) {
            console.log('Strophe connection error.');
        } else if (status == Strophe.Status.CONNECTING) {
            console.log('Strophe is connecting.');
        } else if (status == Strophe.Status.CONNFAIL) {
            console.log('Strophe failed to connect.');
        } else if (status == Strophe.Status.AUTHENTICATING) {
            console.log('Strophe is authenticating.');
        } else if (status == Strophe.Status.AUTHFAIL) {
            console.log('Strophe failed to authenticate.');
        } else if (status == Strophe.Status.CONNECTED) {
            console.log('Strophe is connected.');
            App.connected(this);
        } else if (status == Strophe.Status.DISCONNECTED) {
            console.log('Strophe is disconnected.');
            App.disconnected();
        } else if (status == Strophe.Status.DISCONNECTING) {
            console.log('Strophe is disconnecting.');
        } else if (status == Strophe.Status.ATTACHED) {
            console.log('Strophe is attached.');
        } else {
            console.log('Strophe unknown: ' + status);
        }
    };

    var jid = data.username + "@" + data.domain + "/" + data.resource;
    console.log("Connect: jid: " + jid);
    Session.connection.connect(jid, data.password, connectionCallback);
}