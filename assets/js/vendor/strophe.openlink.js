/**
 * File: strophe.openlink.js
 * A Strophe plugin for Openlink.
 * http://openlink.4ng.net:8080/openlink/xep-xxx-openlink_15-11.xml
 */
Strophe.addConnectionPlugin('openlink', {

    _connection: null,

    profiles: {},
    calls: {},
    callHandlers: {},

    init: function (connection) {
        this._connection = connection;
        console.log('loaded openlink strophe library');
    },

    statusChanged: function(status, condition) {
        var self = this;
        //console.log(status, condition);
        if (status == Strophe.Status.CONNECTED) {
            this.callHandlerId = this._connection.addHandler(function(packet) {
                var callsElem = packet.getElementsByTagName('callstatus')[0];
                var featureElem = packet.getElementsByTagName('devicestatus')[0];
                if (callsElem) {
                    var queryCall = callsElem.getElementsByTagName('call');
                    for (var _i = 0, _len = queryCall.length; _i < _len; _i++) {
                        console.log(queryCall[_i]);
                        var data = self._parseCallInterest(queryCall[_i]);
                        self._updateCalls(data);
                    }
                } else if (featureElem) {
                    var queryFeature = featureElem.getElementsByTagName('feature');
                    for (var _i = 0, _len = queryFeature.length; _i < _len; _i++) {
                        console.log(queryFeature[_i]);
                        var data = self._parsePlayback(queryFeature[_i]);
                        self._updateCalls(data);
                    }
                }
                return true;
            }, null, 'message', null, null, this.getPubsubAddress());
        } else if (status == Strophe.Status.DISCONNECTED) {
            this._connection.removeHandler(this.callHandlerId);
        }
    },

    /**
     * Call this on startup to notify the server that the app is ready to receive events.
     */
    sendPresence: function() {
        this._connection.send($pres());
    },

    /**
     * Implements 'http://xmpp.org/......'.
     * Get Private Storage
     */
    getPrivateData: function (successCallback) {
        var gp_iq = $iq({
            type : "get",
            id: "gtx-data1"
        }).c("query", {
            xmlns : "jabber:iq:private"
        }).c("gtx-profile", {
            xmlns: "http://gltd.net/protocol/gtx/profile"
        });

        var self = this;
        var _successCallback = function(iq) {
            if (successCallback) {
                successCallback(iq);
            } else {
                console.log("Success", iq);
            }
        };

        var _errorCallback = function(iq) {
            console.log("Error", iq);
        };

        this._connection.sendIQ(gp_iq, _successCallback, _errorCallback);
    },

    /**
     * Implements 'http://xmpp.org/extensions/xep-0030.html'.
     * Disco Items
     */
    discoItems: function (to, successCallback, errorCallback) {
        var gp_iq = $iq({
            to : to,
            type : "get",
            id: "items1",
            from: Strophe.getBareJidFromJid(this._connection.jid)
        }).c("query", {
            xmlns : "http://jabber.org/protocol/disco#items"
        });

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }
            if (successCallback) {
                successCallback(iq);
            } else {
                console.log("Success", iq);
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(gp_iq, _successCallback, _errorCallback);
    },

    discoInfo: function (to, successCallback, errorCallback) {
        var gp_iq = $iq({
            to : to,
            type : "get",
            from: Strophe.getBareJidFromJid(this._connection.jid)
        }).c("query", {
            xmlns : "http://jabber.org/protocol/disco#info"
        });

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }
            if (successCallback) {
                //var query = iq.getElementsByTagName('query');
                successCallback(iq);
            } else {
                console.log("Success", iq);
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(gp_iq, _successCallback, _errorCallback);
    },

    /**
     * Implements 'http://xmpp.org/protocol/openlink:01:00:00#get-profiles'.
     * @param to Openlink XMPP component.
     * @param successCallback called on successful execution with array of profile IDs and profiles.
     * @param errorCallback called on error.
     */
    getProfiles: function (to, resource, login, deviceNum, directoryNumber, successCallback, errorCallback, bareJid) {
        var bareJid = bareJid? bareJid:Strophe.getBareJidFromJid(this._connection.jid);
        bareJid = bareJid.toLowerCase();
        var login = login === true || login === "true"? true:false;
        var gp_iq = $iq({
            to : to,
            type : "set",
            from : Strophe.getBareJidFromJid(this._connection.jid) + "/" + resource
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#get-profiles"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in").c("jid").t(bareJid + "/" + resource).up()
        .c("mobility", {
            login: login
        });
        if (deviceNum !== "" && login) {
            gp_iq.attrs({
                devicenum : deviceNum
            })
        };
        if (directoryNumber !== "" && login) {
            gp_iq.attrs({
                directorynumber: directoryNumber
            })
        };

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }
            self.profiles = {};
            var query = iq.getElementsByTagName('profile');

            for (var _i = 0, _len = query.length; _i < _len; _i++) {
                var data = self._parseProfile(query[_i]);
                var profile = new Profile(data);
                for (var _j = 0, _len1 = data.actions.length; _j < _len1; _j++) {
                    profile._addAction(data.actions[_j]);
                }
                console.log(profile);

                self.profiles[profile.id] = profile;
            }
            if (successCallback) {
                successCallback(self.profiles);
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(gp_iq, _successCallback, _errorCallback);
    },

    /**
     * Implements 'http://xmpp.org/protocol/openlink:01:00:00#logon-device'.
     * @param to Openlink XMPP component.
     * @param successCallback called on successful execution with array of profile IDs and profiles.
     * @param errorCallback called on error.
     */
    logonDevice: function (to, resource, login, deviceNum, directoryNumber, successCallback, errorCallback, bareJid) {
        var bareJid = bareJid? bareJid:Strophe.getBareJidFromJid(this._connection.jid);
        bareJid = bareJid.toLowerCase();
        var login = login === true || login === "true"? true:false;
        var gp_iq = $iq({
            to : to,
            type : "set",
            from : Strophe.getBareJidFromJid(this._connection.jid) + "/" + resource
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#logon-device"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in")
        .c("jid").t(bareJid + "/" + resource).up()
        .c("mobility", {
            login: login
        });
        if (deviceNum !== "" && login) {
            gp_iq.attrs({
                devicenum : deviceNum
            })
        };
        if (directoryNumber !== "" && login) {
            gp_iq.attrs({
                directorynumber: directoryNumber
            })
        };

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }

            var command = iq.getElementsByTagName('command')[0];

            if (command) {
                var commandData = self._parseAttributes(command);
            }
            if (successCallback) {
                successCallback(commandData.status);
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(gp_iq, _successCallback, _errorCallback);
    },

    /**
     * Implements 'http://xmpp.org/protocol/openlink:01:00:00#get-interests'.
     * @param to Openlink XMPP component.
     * @param profileId profile ID.
     * @param successCallback called on successful execution.
     * @param errorCallback called on error.
     */
    getInterests: function(to, profileId, successCallback, errorCallback) {
        var interests = {};

        var gi_iq = $iq({
            to : to,
            type : "set"
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#get-interests"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in").c("profile").t(profileId);

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }

            var interestsElem = iq.getElementsByTagName('interests')[0];
            var callsElem = iq.getElementsByTagName('callstatus')[0];
            if (interestsElem) {
                var query = interestsElem.getElementsByTagName('interest');
                for (var _i = 0, _len = query.length; _i < _len; _i++) {
                    var data = self._parseAttributes(query[_i]);
                    if (data.id) {
                        var interest = new Interest(data);
                        interests[interest.id] = interest;
                    }
                }
                if (callsElem) {
                    var queryCall = callsElem.getElementsByTagName('call');
                    for (var _i = 0, _len = queryCall.length; _i < _len; _i++) {
                        console.log(queryCall[_i]);
                        var data = self._parseCallInterest(queryCall[_i]);
                        self._updateCalls(data);
                    }
                }
                if (successCallback) {
                    successCallback(interests);
                }
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(gi_iq, _successCallback, _errorCallback);
    },

    /**
     * Implements 'http://xmpp.org/protocol/openlink:01:00:00#get-interest'.
     * @param to Openlink XMPP component.
     * @param interestId interest ID.
     * @param successCallback called on successful execution.
     * @param errorCallback called on error.
     */
    getInterest: function(to, interestId, successCallback, errorCallback) {
        var interests = {};

        var gi_iq = $iq({
            to : to,
            type : "set"
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#get-interest"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in").c("interest").t(interestId);

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }

            var interestsElem = iq.getElementsByTagName('interests')[0];
            var callsElem = iq.getElementsByTagName('callstatus')[0];
            if (interestsElem) {
                var query = interestsElem.getElementsByTagName('interest');
                for (var _i = 0, _len = query.length; _i < _len; _i++) {
                    var data = self._parseAttributes(query[_i]);
                    if (data.id) {
                        var interest = new Interest(data);
                        interests[interest.id] = interest;
                    }
                }
                if (callsElem) {
                    var queryCall = callsElem.getElementsByTagName('call');
                    for (var _i = 0, _len = queryCall.length; _i < _len; _i++) {
                        console.log(queryCall[_i]);
                        var data = self._parseCallInterest(queryCall[_i]);
                        self._updateCalls(data);
                    }
                }
                if (successCallback) {
                    successCallback(interests);
                }
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(gi_iq, _successCallback, _errorCallback);
    },

    /**
     * Implements 'http://xmpp.org/protocol/openlink:01:00:00#get-features'.
     * @param to Openlink XMPP component.
     * @param profileId profile ID.
     * @param successCallback called on successful execution.
     * @param errorCallback called on error.
     */
    getFeatures: function(to, profileId, successCallback, errorCallback) {
        var features = {};
        var gf_iq = $iq({
            to : to,
            type : "set"
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#get-features"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in").c("profile").t(profileId);

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }

            var query = iq.getElementsByTagName('feature');
            for (var _i = 0, _len = query.length; _i < _len; _i++) {
                var data = self._parseAttributes(query[_i]);
                var feature = new Feature(data);
                features[feature.id] = feature;
            }

            if (successCallback) {
                successCallback(features);
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(gf_iq, _successCallback, _errorCallback);
    },

    /**
     * Implements 'http://xmpp.org/protocol/openlink:01:00:00#set-features'.
     * @param to Openlink XMPP component.
     * @param profileId profile ID.
     * @param successCallback called on successful execution.
     * @param errorCallback called on error.
    */

    setFeatures: function(to, profileId, feature, value1, value2, value3, successCallback, errorCallback) {
        var features = {};
        var gf_iq = $iq({
            to : to,
            type : "set"
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#set-features"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in")
        .c("profile").t(profileId).up()
        .c("feature").t(feature).up()
        .c("value1").t(value1).up()
        .c("value2").t(value2).up()
        .c("value3").t(value3).up();

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }

            if (successCallback) {
                successCallback("Feature successfully set");
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(gf_iq, _successCallback, _errorCallback);
    },

    /**
     * Implements 'http://xmpp.org/protocol/openlink:01:00:00#get-call-history'.
     * @param to Openlink XMPP component.
     * @param successCallback called on successful execution.
     * @param errorCallback called on error.
     */
    getCallHistory: function(to, bareJid, profile, caller, called, calltype, fromdate, uptodate, start, count, successCallback, errorCallback) {
        var bareJid = bareJid? bareJid:Strophe.getBareJidFromJid(this._connection.jid);
        bareJid = bareJid.toLowerCase();
        var history = {};
        var self = this;
        var gf_iq = $iq({
            to : to,
            type : "set"
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#get-call-history"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in")
        .c("jid").t(bareJid).up()
        .c("profile").t(profile).up()
        .c("caller").t(caller).up()
        .c("called").t(called).up()
        .c("calltype").t(calltype).up()
        .c("fromdate").t(fromdate).up()
        .c("uptodate").t(uptodate).up()
        .c("start").t(start).up()
        .c("count").t(count).up();
        
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }

            var query = iq.getElementsByTagName('call');
            for (var _i = 0, _len = query.length; _i < _len; _i++) {
                var data = self._parseCallHistory(query[_i]);
                history[data.id] = data;
            }
            console.log("HISTORY:", history);
            if (successCallback) {
                successCallback(history);
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(gf_iq, _successCallback, _errorCallback);
    },

    /**
     * Implements 'http://www.xmpp.org/extensions/xep-0060.html#subscriber-subscribe'.
     * @param to pubsub service, usually pubsub.domain - see getPubsubAddress().
     * @param interest the Openlink interest/pubsub node.
     * @param successCallback called on successful execution.
     * @param errorCallback called on error.
     */
    subscribe: function(to, interest, successCallback, errorCallback) {
        var self = this;
        var subs_iq2 = $iq({
            to: to,
            type: "set"
        }).c('pubsub', {
            xmlns: "http://jabber.org/protocol/pubsub"
        }).c('subscribe', {
            node: interest,
            jid: Strophe.getBareJidFromJid(self._connection.jid)
        });
        
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }

            if (successCallback) {
                successCallback(iq);
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback) {
                errorCallback('No subscriptions exist');
            }
        };

        self._connection.sendIQ(subs_iq2, _successCallback, _errorCallback);
    },

    /**
     * Implements 'http://www.xmpp.org/extensions/xep-0060.html#subscriber-unsubscribe'.
     * @param to pubsub service, usually pubsub.domain - see getPubsubAddress().
     * @param interest the Openlink interest/pubsub node.
     * @param successCallback called on successful execution.
     * @param errorCallback called on error.
     */
    unsubscribe: function(to, interest, subid, successCallback, errorCallback) {
        var self = this;
        var subs_iq = $iq({
            to: to,
            type: "set"
        }).c('pubsub', {
            xmlns: "http://jabber.org/protocol/pubsub"
        }).c('unsubscribe', {
            node: interest,
            jid: Strophe.getBareJidFromJid(self._connection.jid),
            subid: subid
        });
        
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }

            if (successCallback) {
                successCallback('Unsubscribed');
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback) {
                errorCallback('No subscriptions exist');
            }
        };

        self._connection.sendIQ(subs_iq, _successCallback, _errorCallback);
    },

    /**
     * Implements 'http://www.xmpp.org/extensions/xep-0060.html#entity-subscriptions'.
     * @param to pubsub service, usually pubsub.domain - see getPubsubAddress().
     * @param interest the Openlink interest/pubsub node.
     * @param successCallback called on successful execution with list of subscriptions.
     * @param errorCallback called on error.
     */
    getSubscriptions: function(to, interest, successCallback, errorCallback) {
        var subscriptions = [];
        var subs_iq = $iq({
            to: to,
            type: "get"
        }).c('pubsub', {
            xmlns: "http://jabber.org/protocol/pubsub"
        }).c('subscriptions', {
            node: interest
        });

        var self = this;
        var _successCallback = function(iq) {
            var query = iq.getElementsByTagName('subscription');

            for (var _i = 0, _len = query.length; _i < _len; _i++) {
                var data = self._parseAttributes(query[_i]);
                subscriptions.push(data);
            }
            if (successCallback) {
                successCallback(subscriptions);
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback) {
                errorCallback('Error getting subscriptions');
            }
        };

        this._connection.sendIQ(subs_iq, _successCallback, _errorCallback);
    },

    /**
     * Returns the default pubsub address on the XMPP servers (tested on Openfire).
     * @returns {string} pubsub component address.
     */
    getPubsubAddress: function() {
        return 'pubsub.' + this._connection.domain;
    },

    /**
     * Implements 'http://xmpp.org/protocol/openlink:01:00:00#make-call'.
     * @param to Openlink XMPP component.
     * @param interest the Openlink interest.
     * @param extension the far party extension.
     * @param features array of call features in the form Feature.id, Feature.value1 and Feature.value2.
     * @param successCallback called on successful execution with new call object.
     * @param errorCallback called on error.
     */
    makeCall: function(to, interest, extension, features, successCallback, errorCallback, bareJid) {
        var bareJid = bareJid? bareJid:Strophe.getBareJidFromJid(this._connection.jid);
        bareJid = bareJid.toLowerCase();
        console.log("Make call to: " + extension + ", callback: " + interest + ", for: " + bareJid);
        
        var mc_iq = $iq({
            to : to,
            type : "set"
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#make-call"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in")
            .c("jid").t(bareJid).up()
            .c("interest").t(interest).up()
            .c("destination").t(extension);

        if (features && features.length > 0) {
            mc_iq = mc_iq.up().c("features");
            for (var _i = 0, _len = features.length; _i < _len; _i++) {
                mc_iq = mc_iq.c("feature").c("id").t(features[_i].id).up()
                    .c("value1").t(features[_i].value1).up();
                if (features[_i].value2) {
                    mc_iq = mc_iq.c("value2").t(features[_i].value2).up();
                }
                mc_iq.up();
            }
        }

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }

            var call = self._parseCall(iq);
            if (successCallback) {
                successCallback(call);
            }
        };

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(mc_iq, _successCallback, _errorCallback);
    },
    
    /**
     * Implements 'http://xmpp.org/protocol/openlink:01:00:00#request-action'.
     * @param to Openlink XMPP component.
     * @param interest the Openlink interest.
     * @param action the Openlink action in the form Action.id, Action.value1, Action.value2.
     * @param successCallback called on successful execution.
     * @param errorCallback called on error.
     */
    requestAction: function(to, interest, callId, action, successCallback, errorCallback) {
        var mc_iq = $iq({
            to : to,
            from : Strophe.getBareJidFromJid(this._connection.jid),
            type : "set"
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#request-action"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in")
        .c("interest").t(interest).up()
        .c("action").t(action.id).up()
        .c("call").t(callId).up();
        if (action.value1) {
            mc_iq = mc_iq.c("value1").t(action.value1).up();
        }
        if (action.value2) {
            mc_iq = mc_iq.c("value2").t(action.value2).up();
        }

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }
            var call = self._parseCall(iq);
            if (successCallback) {
                successCallback(call);
            }
        }

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(mc_iq, _successCallback, _errorCallback);
    },

    manageVoiceMessageRecord: function(to, profile, label, successCallback, errorCallback) {
        console.log("Manage voice message RECORD: " + label);
        var mvm_iq = $iq({
            to : to,
            type : "set",
            id : 'recordId'
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#manage-voice-message"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in").c("profile").t(profile).up().c("action").t("Record").up().c("label").t(label);

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }
            var voiceMessage = self._parseVoiceMessage(iq);
            if (successCallback) {
                successCallback(voiceMessage);
            }
        }
        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(mvm_iq, _successCallback, _errorCallback);
    },

    manageVoiceMessagePlayback: function(to, profile, feature, successCallback, errorCallback) {
        console.log("Manage voice message PLAYBACK: " + feature);
        var mvm_iq = $iq({
            to : to,
            type : "set",
            id : 'recordId'
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#manage-voice-message"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in").c("profile").t(profile).up().c("action").t("Playback").up().c("features").c("feature").c(
                "id").t(feature);

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }
            var voiceMessage = self._parseVoiceMessage(iq);
            if (successCallback) {
                successCallback(voiceMessage);
            }
        }

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(mvm_iq, _successCallback, _errorCallback);
    },

    manageVoiceMessageQuery: function(to, profile, feature, successCallback, errorCallback) {
        console.log("Manage voice message QUERY: " + feature);
        var mvm_iq = $iq({
            to : to,
            type : "set",
            id : 'recordId'
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#manage-voice-message"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in").c("profile").t(profile).up().c("action").t("Query").up().c("features").c("feature").c(
                "id").t(feature);

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }
            var voiceMessage = self._parseVoiceMessageQuery(iq);
            if (successCallback) {
                successCallback(voiceMessage);
            }
        }

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(mvm_iq, _successCallback, _errorCallback);
    },

    manageVoiceMessageArchive: function(to, profile, feature, successCallback, errorCallback) {
        console.log("Manage voice message ARCHIVE: " + feature);
        var mvm_iq = $iq({
            to : to,
            type : "set",
            id : 'archiveId'
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#manage-voice-message"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in").c("profile").t(profile).up().c("action").t("Archive").up().c("features").c("feature").c(
                "id").t(feature);

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }
            //var voiceMessage = self._parseVoiceMessageQuery(iq);
            if (successCallback) {
                successCallback(iq);
            }
        }

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(mvm_iq, _successCallback, _errorCallback);
    },

    manageVoiceBlast: function(to, profile, interest, keys, dests, offset, successCallback, errorCallback) {
        console.log("Voice Blast : " + keys.toString() + ", Destinations: " + dests.toString() + ", Offset: " + offset);

        var mvb_iq = $iq({
            to : to,
            type : "set",
            id : 'blastId'
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#manage-voice-blast"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        });

        var vb_id = "VB" + new Date().getTime();
        mvb_iq.c("in").c("profile").t(profile);
        mvb_iq.up().c("interest").t(interest);
        mvb_iq.up().c("blastid").t(vb_id);
        mvb_iq.up().c("action").t("Create");
        mvb_iq.up().c("destinations");

        // add destinations
        for ( var i = 0; i < dests.length; i++) {
            mvb_iq.c("destination").t(dests[i]).up();
        }

        mvb_iq.up().c("features");

        // add message keys
        for ( var i = 0; i < keys.length; i++) {
            mvb_iq.c("feature").c("id").t(keys[i]).up().up();
        }

        mvb_iq.up().c("timestamp").t("" + offset);

        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }
            //var voiceMessage = self._parseVoiceMessage(iq);
            if (successCallback) {
                successCallback(iq);
            }
        }

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(mvb_iq, _successCallback, _errorCallback);
    },

    manageVoiceMessageSave: function(to, profile, label, location, creationdate, successCallback, errorCallback) {
        console.log("Manage voice message SAVE: " + label);
        var mvm_iq = $iq({
            to : to,
            type : "set",
            id : 'saveId'
        }).c("command", {
            xmlns : "http://jabber.org/protocol/commands",
            action : "execute",
            node : "http://xmpp.org/protocol/openlink:01:00:00#manage-voice-message"
        }).c("iodata", {
            xmlns : "urn:xmpp:tmp:io-data",
            type : "input"
        }).c("in").c("profile").t(profile).up().c("action").t("Save").up().c("label").t(label).up()
            .c("audiofiles", {
                xmlns : "http://xmpp.org/protocol/openlink:01:00:00/audio-files"
            }).c("audiofile");

        mvm_iq.c("id").t(profile);
        mvm_iq.up().c("msglen").t("902");
        mvm_iq.up().c("label").t(label);
        mvm_iq.up().c("size").t("1024");
        mvm_iq.up().c("creationdate").t(creationdate);
        mvm_iq.up().c("lifetime").t("0");
        mvm_iq.up().c("location", {
            url : location
        }).c("auth", {
            type : "none"
        }).up();
        mvm_iq.up().c("properties").t("");

        var self = this;
        var _successCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getErrorNote(iq));
                return;
            }
            var voiceMessage = self._parseVoiceMessage(iq);
            if (successCallback) {
                successCallback(voiceMessage);
            }
        }

        var _errorCallback = function(iq) {
            if (errorCallback && self._isError(iq)) {
                errorCallback(self._getError(iq));
            }
        };

        this._connection.sendIQ(mvm_iq, _successCallback, _errorCallback);
    },

    _updateCalls: function(callEv) {
        console.log("CALLS UPDATED");
        if (callEv) {
            var id = callEv.id;
            var msgid = callEv.msgid;
            if (id || msgid) {
                if (id) {
                    this.calls[id] = callEv;
                }
                var changed = callEv.changed;
                // notify call handlers here
                for (var handler in this.callHandlers) {
                    this.callHandlers[handler](callEv, changed);
                }

                if (callEv.state === 'ConnectionCleared' || callEv.state === 'CallMissed') {
                    delete this.calls[id];
                }
            }
        }
    },

    _getUid: function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    },

    /**
     * Adds a call handler which is notified on any call event with the parameters: Call Event and Changed Element.
     * @param handler
     * @returns {*} the id associated with the handler. Retain this in order to remove the handler.
     */
    addCallHandler: function(handler) {
        var id = this._getUid();
        this.callHandlers[id] = handler;
        return id;
    },

    /**
     * Removes the call handler.
     * @param id the call handler id.
     * @returns {boolean}
     */
    removeHandler: function(id) {
        return delete this.callHandlers[id];
    },

    /* Parser Helpers */
    _parseDiscoInfo: function(query) {
        var data = [];
        if (query) {
            for (var _i = 0, _len = query.childNodes.length; _i < _len; _i++) {
                var data2 = {}
                var element = query.childNodes[_i];
                data2[element.tagName] = {};
                for (var _j = 0, _len1 = element.attributes.length; _j < _len1; _j++) {
                    var attribute = element.attributes[_j];
                    data2[element.tagName][attribute.nodeName] = attribute.nodeValue;
                }
                data.push(data2);
            }
        }
        return data;
    },

    _parseProfile: function(profile) {
        var data = {};
        data.actions = [];
        if (profile) {
            var a = profile.attributes;
            if (a) {
                if (a.id && a.device) {
                    for (var _i = 0, _len = a.length; _i < _len; _i++) {
                        data[a[_i].name] = a[_i].value;
                    }
                    var query = profile.getElementsByTagName('action');
                    for (var _j = 0, _len1 = query.length; _j < _len1; _j++) {
                        var action = this._parseAttributes(query[_j]);
                        data.actions.push(action);
                    }
                }
            }
        }
        return data;
    },

    _parseAttributes: function(elem) {
        var data = {};
        if (elem) {
            var a = elem.attributes;
            if (a) {
                for (var _i = 0, _len = a.length; _i < _len; _i++) {
                    data[a[_i].name] = a[_i].value;
                }
            }
        }
        return data;
    },

    _flattenElementAndText: function(elem) {
        var data = {};
        if (elem) {
            for (var _i = 0; _i < elem.childNodes.length; _i++) {
                data[elem.childNodes[_i].tagName] = elem.childNodes[_i].textContent;
            }
        }
        return data;
    },

    _parseVRXml: function(elem) {
        var data = {};
        elem = elem.getElementsByTagName('VoiceRecording')[0];
        if (elem) {
            for (var _i = 0; _i < elem.childNodes.length; _i++) {
                var child = elem.childNodes[_i];
                if (child.children.length < 1) {
                    data[child.tagName] = child.textContent;
                } else {
                    var data2 = {};
                    for (var _j = 0; _j < child.childNodes.length; _j++) {
                        var child2 = child.childNodes[_j];
                        if (child2.children.length < 1) {
                            data2[child2.tagName] = child2.textContent;
                        } else {
                            var data3 = {};
                            for (var _k = 0; _k < child2.childNodes.length; _k++) {
                                var child3 = child2.childNodes[_k];
                                data3[child3.tagName] = child3.textContent;
                            }
                            data2[child2.tagName] = data3;
                        }
                    }
                    data[child.tagName] = data2;
                }
            }
        }
        return data;
    },

    _parseGroupXml: function(elem) {
        var data = [];
        elem = elem.childNodes[0];
        if (elem) {
            for (var _i = 0; _i < elem.childNodes.length; _i++) {
                var child = elem.childNodes[_i];
                if (child.children.length < 1) {
                    data[child.tagName] = child.textContent;
                } else {
                    var data2 = {};
                    for (var _j = 0; _j < child.childNodes.length; _j++) {
                        var child2 = child.childNodes[_j];
                        if (child2.children.length < 1) {
                            data2[child2.tagName] = child2.textContent;
                        }
                    }
                    data[_i] = data2;
                }
            }
        }
        return data;
    },

    _parseMemberXml: function(elem) {
        var data = [];
        elem = elem.childNodes[0];
        for (var _h = 0; _h < elem.childNodes.length; _h++) {
            var attribute = elem.childNodes[_h];
            if (attribute.nodeName === "members") {
                elem = attribute;
            }
        }
        if (elem) {
            for (var _i = 0; _i < elem.childNodes.length; _i++) {
                var child = elem.childNodes[_i];
                if (child.children.length < 1) {
                    var jid = child.textContent;
                    jid = jid.split('@')[0];
                    data.push(jid);
                }
            }
        }
        return data;
    },

    _parseCallParticipants: function(elem) {
        var data = [];
        if (elem) {
            var participants = elem.childNodes;
            for (var _i = 0; _i < participants.length; _i++) {
                var participant = participants[_i];
                data.push(this._parseAttributes(participant));
                if (participant.childNodes.length > 0) {
                    var participations = this._parseCallParticipations(participant.childNodes[0]);
                    data[_i]["participations"] = participations;
                }
            }
        }
        return data;
    },

    _parseCallParticipations: function(elem) {
        var data = [];
        var legs = elem.childNodes;
        for (var _i = 0; _i < legs.length; _i++) {
            data.push(this._parseAttributes(legs[_i]));
        }
        return data;
    },

    _parseOriginatorRef: function(elem) {
        var data = {};
        if (elem) {
            for (var _i = 0; _i < elem.childNodes.length; _i++) {
                data[elem.childNodes[_i].id] = elem.childNodes[_i].textContent;
            }
        }
        return data;
    },

    _parseCallFeatures: function(elem) {
        var data = {};
        if (elem) {
            for (var _i = 0; _i < elem.childNodes.length; _i++) {
                data[elem.childNodes[_i].attributes.id.value] = elem.childNodes[_i].textContent;
            }
        }
        return data;
    },

    _parseCallActions: function(elem) {
        var data = [];
        if (elem) {
            for (var _i = 0; _i < elem.childNodes.length; _i++) {
                data.push(elem.childNodes[_i].tagName);
            }
        }
        return data;
    },

    _parseCallHistory: function(elem) {
        var data = {};
        if (elem) {
            for (var _i = 0; _i < elem.childNodes.length; _i++) {
                data[elem.childNodes[_i].nodeName] = elem.childNodes[_i].textContent;
            }
        }
        return data;
    },

    _getElementText: function(name, elem) {
        var result; 
        if (name && elem) {
            var foundElem = elem.getElementsByTagName(name)[0];
            if (name === 'playlist') {
                result = elem.textContent
            } else if (foundElem) {
                result = foundElem.textContent;
            }
        }
        return result;
    },

    _parseCall: function(elem) {
        var call = null;
        if (elem) {
            var callStatus = elem.getElementsByTagName('callstatus')[0];
            if (callStatus) {
                var callElem = callStatus.getElementsByTagName('call')[0];
                    if (callElem) {
                        var id = this._getElementText('id', callElem);
                        var call = new Call({id: id});

                        call.ref = this._parseOriginatorRef(callElem.getElementsByTagName('originator-ref')[0]);

                        call.profile = this._getElementText('profile', callElem);
                        call.interest = this._getElementText('interest', callElem);
                        call.changed = this._getElementText('changed', callElem);
                        call.state = this._getElementText('state', callElem);

                        call.direction = this._getElementText('direction', callElem);
                        call.duration = this._getElementText('duration', callElem);

                        call.caller = this._flattenElementAndText(callElem.getElementsByTagName('caller')[0]);

                        call.called = this._flattenElementAndText(callElem.getElementsByTagName('called')[0]);

                        call.actions = this._parseCallActions(callElem.getElementsByTagName('actions')[0]);

                        call.participants = this._parseCallParticipants(callElem.getElementsByTagName('participants')[0]);
                        call.features = this._parseCallFeatures(callElem.getElementsByTagName('features')[0]);
                    }
            }
        }
        return call;
    },

    _parseVoiceMessage: function(elem) {
        var voiceMessage = {};
        if (elem) {
            var deviceStatus = elem.getElementsByTagName('devicestatus')[0];
            if (deviceStatus) {
                var deviceElem = deviceStatus.getElementsByTagName('feature')[0];
                if (deviceElem) {
                    voiceMessage.feature = deviceElem.id;
                    voiceMessage.status = this._getElementText('status', deviceElem);
                    voiceMessage.action = this._getElementText('action', deviceElem);
                    voiceMessage.exten = this._getElementText('exten', deviceElem);
                    voiceMessage.label = this._getElementText('label', deviceElem);
                }
            }
        }
        return voiceMessage;
    },

    _parseVoiceMessageQuery: function(elem) {
        var voiceMessage = {};
        if (elem) {
            var deviceStatus = elem.getElementsByTagName('devicestatus')[0];
            if (deviceStatus) {
                var deviceElem = deviceStatus.getElementsByTagName('feature')[0];
                if (deviceElem) {
                    voiceMessage.feature = deviceElem.id;
                    voiceMessage.label = this._getElementText('label', deviceElem);
                    voiceMessage.status = this._getElementText('status', deviceElem);
                    voiceMessage.msglen = this._getElementText('msglen', deviceElem);
                    voiceMessage.creationdate = this._getElementText('creationdate', deviceElem);
                    voiceMessage.playlists = [];
                    playlistsElem = deviceElem.getElementsByTagName('playlists')[0];
                    if (playlistsElem) {
                        for (var _i = 0; _i < playlistsElem.childNodes.length; _i++) {
                            voiceMessage.playlists.push(this._getElementText('playlist', playlistsElem.childNodes[_i]));
                        }
                    }
                }
            }
        }
        return voiceMessage;
    },

    _parsePlayback: function(elem) {
        var voiceMessage = {};
        if (elem) {
            var featureElem = elem;
            voiceMessage.msgid = this._parseAttributes(featureElem).id;

            voiceMessage.callid = this._getElementText('callid', featureElem);
            voiceMessage.status = this._getElementText('status', featureElem);
            voiceMessage.action = this._getElementText('action', featureElem);
            voiceMessage.msglen = this._getElementText('msglen', featureElem);
            voiceMessage.state = this._getElementText('state', featureElem);
            voiceMessage.exten = this._getElementText('exten', featureElem);
            voiceMessage.creationdate = this._getElementText('creationdate', featureElem);
            voiceMessage.playprogress = this._getElementText('playprogress', featureElem);
        }
        return voiceMessage;
    },

    _parseCallInterest: function(elem) {
        var call = null;
        if (elem) {
            var callElem = elem;
            var id = this._getElementText('id', callElem);
            var call = new Call({id: id});

            call.ref = this._parseOriginatorRef(callElem.getElementsByTagName('originator-ref')[0]);

            call.profile = this._getElementText('profile', callElem);
            call.interest = this._getElementText('interest', callElem);
            call.changed = this._getElementText('changed', callElem);
            call.state = this._getElementText('state', callElem);

            call.direction = this._getElementText('direction', callElem);
            call.duration = this._getElementText('duration', callElem);

            call.caller = this._flattenElementAndText(callElem.getElementsByTagName('caller')[0]);

            call.called = this._flattenElementAndText(callElem.getElementsByTagName('called')[0]);

            call.actions = this._parseCallActions(callElem.getElementsByTagName('actions')[0]);

            call.participants = this._parseCallParticipants(callElem.getElementsByTagName('participants')[0]);
            call.features = this._parseCallFeatures(callElem.getElementsByTagName('features')[0]);
        }
        return call;
    },

    _getErrorNote: function(iq) {
        var errorNote = [];
        var noteElem = iq.getElementsByTagName('note');
        if (noteElem) {
            for (var _i = 0; _i < noteElem.length; _i++) {
                if (noteElem[_i].attributes.type && noteElem[_i].attributes.type.value == 'error') {
                    if (noteElem[_i].textContent && noteElem[_i].textContent.length > 0)
                        errorNote["errorNote"] = noteElem[_i].textContent;
                }
            };
        }
        var attributes = iq.attributes;
        if (attributes) {
            for (var _j = 0; _j < attributes.length; _j++) {
                if (attributes[_j].nodeName === "from") {
                    var from = attributes[_j].textContent;
                    errorNote["from"] = from;
                }
            }
        }
        return errorNote;
    },

    _getError: function(iq) {
        var errorIq = [];
        var errorElem = iq.getElementsByTagName('error')[0];
        if (errorElem) {
            for (var _i = 0; _i < errorElem.childNodes.length; _i++) {
                var errorElemChildNode = errorElem.childNodes[_i];
                if (errorElemChildNode.nodeName === "error") {
                    var errorCause = errorElemChildNode.childNodes[0].nodeName;
                    errorIq["errorIq"] = errorCause;
                } else if (errorElemChildNode) {
                    var errorCause = errorElemChildNode.nodeName;
                    errorIq["errorIq"] = errorCause;
                }
            }
        }
        var attributes = iq.attributes;
        if (attributes) {
            for (var _j = 0; _j < attributes.length; _j++) {
                if (attributes[_j].nodeName === "from") {
                    var from = attributes[_j].textContent;
                    errorIq["from"] = from;
                }
            }
        }
        return errorIq;
    },

    _isError: function(elem) {
        var error = false;
        if (elem) {
            var foundElem = elem.getElementsByTagName('note');
            if (foundElem && foundElem.length > 0) {
                for (var _i = 0; _i < foundElem.length; _i++) {
                    if (foundElem[_i].attributes.type && foundElem[_i].attributes.type.value == 'error') {
                        if (foundElem[_i].textContent && foundElem[_i].textContent.length > 0) {
                            console.error(foundElem[_i].textContent);
                            error = true;
                        }
                    }
                };
                return error;
            }
            else if (elem.attributes.type.value === 'error') {
                return error = true;
            }
        }
    }

});

function Profile(data) {
    this.actions = {};
    this.interests = {};
    for (var elem in data) {
        this[elem] = data[elem];
    }
}

Profile.prototype._addAction = function(data) {
    var action = new Action(data);
    this.actions[action.id] = action;
    return action;
};

Profile.prototype._addInterest = function(data) {
    var interest = new Interest(data);
    this.interests[interest.id] = interest;
    return interest;
};

function Action(data) {
    for (var elem in data) {
        this[elem] = data[elem];
    }
}

function Interest(data) {
    for (var elem in data) {
        this[elem] = data[elem];
    }
}

function Feature(data) {
    for (var elem in data) {
        this[elem] = data[elem];
    }
}

function Call(data, caller, called, actions, participants, features) {
    this.id = data.id;
    this.profile = data.profile;
    this.interest = data.interest;
    this.changed = data.changed;
    this.state = data.state;
    this.direction = data.direction;
    this.duration = data.duration;
    this.caller = caller;
    this.called = called;
    this.actions = actions;
    this.participants = participants;
    this.features = features;
}

Call.prototype._update = function(data, caller, called, actions, participants, features) {
    this.profile = data.profile;
    this.interest = data.interest;
    this.changed = data.changed;
    this.state = data.state;
    this.direction = data.direction;
    this.duration = data.duration;
    this.caller = caller;
    this.called = called;
    this.actions = actions;
    this.participants = participants;
    this.features = features;
}